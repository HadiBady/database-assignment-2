USE mbadawi3assign2db;

-- delete doctors that got the license before January 1st 1965.
DELETE FROM doctor WHERE datelicense < '1965-01-01';
-- the previous command causes an error  because at least one of the doctor is referenced in another table as a foreign key, an since this table did not have ON DELETE CASCADE.
-- now we can't remove something unless we remove the from hospital which references that doctor


-- Create a view that shows the patients first names and last names who are treated by head doctors of the hospitals, 
-- which became heads after Dec 31, 1995. The doctor's last name and the hospital name were shown as well. 
CREATE VIEW part4 AS SELECT patient.firstname, patient.lastname, doctor.lastname AS "doctorname", hosname FROM patient, doctor JOIN hospital WHERE licnum IN (SELECT headdoclicnum FROM hospital WHERE headdocstartdate > '1995-12-31') AND licnum = headdoclicnum AND ohip IN (SELECT patientohip FROM doctorpatient WHERE doclicnum = licnum);

-- show that the view was successful by listing the rows in it
SELECT * FROM part4;

-- Show all the rows in the doctor and doctorpatient table
SELECT * FROM doctor;
SELECT * FROM doctorpatient;

-- Delete any doctor with 'Clooney' as a last name
DELETE FROM doctor WHERE lastname = 'Clooney';

-- showing the doctors table again to demonstate the previous command's success. 
SELECT * FROM doctor;

-- Show what the cascade of the doctorpatient table
SHOW CREATE TABLE doctorpatient;

-- show the rows of the doctor table again
SELECT * FROM doctor;

-- delete all the Surgeons
DELETE FROM doctor WHERE specialty = 'Surgeon';
-- the delete command did not run successfully as doctor Aziz id is referenced as a foreign key in the hospital table as the head doctor.
-- So we cannot remove him unless he is no longer a head doctor since the table was built to always have a head doctor. 

